<?php
class Person
{

    public $name;
    public $address;
    public $phone;

    public function __wakeup()
    {
        $this->doSomething();
    }

    public function doSomething(){
        echo "I'm doing something<br>";
    }
}


$obj = new Person();
$myVar = serialize($obj);

echo "<br>";

$newObj = unserialize($myVar);
?>